<?php

namespace Drupal\apidrupalorg\Form;

use Drupal\api\Entity\Branch;
use Drupal\api\Entity\DocBlock;
use Drupal\api\Entity\Project;
use Drupal\api\Formatter;
use Drupal\comment\Entity\Comment;
use Drupal\Component\Utility\Environment;
use Drupal\Core\Batch\BatchBuilder;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\user\Entity\User;

/**
 * Import form for apidrupalorg.
 */
class ImportForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'apidrupalorg_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // @codingStandardsIgnoreStart
    $form['intro'] = [
      '#markup' => '<div>' .
        $this->t('Import comments and users from a XML file exported via @export_link.', [
          '@export_link' => Link::fromTextAndUrl(
            'api.drupal.org (Drupal 7) - Comment export',
            Url::fromUri('https://api.drupal.org/comment-management/export', [
              'attributes' => [
                'target' => '_blank',
              ]
            ])
          )->toString(),
        ]) . '<br />' .
        $this->t('It will create all the users and comments made in pages that are present on this site.') . '<br />' .
        $this->t('Existing records will not be updated.') .
        '</div>',
    ];
    // @codingStandardsIgnoreEnd

    $validators = [
      'file_validate_extensions' => ['xml'],
      'file_validate_size' => [Environment::getUploadMaxSize()],
    ];
    $form['import_file'] = [
      '#type' => 'file',
      '#title' => $this->t('XML File'),
      '#description' => [
        '#theme' => 'file_upload_help',
        '#description' => $this->t('XML file produced by the Drupal 7 version of the api.drupal.org site.'),
        '#upload_validators' => $validators,
      ],
      '#size' => 50,
      '#upload_validators' => $validators,
      '#attributes' => ['class' => ['file-import-input']],
    ];

    $form['actions'] = [
      '#type' => 'actions',
    ];

    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
      '#button_type' => 'primary',
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $all_files = $this->getRequest()->files->get('files', []);
    if (!empty($all_files['import_file'])) {
      $file_upload = $all_files['import_file'];
      if ($file_upload->isValid()) {
        $form_state->setValue('import_file', $file_upload->getRealPath());
        return;
      }
    }

    $form_state->setErrorByName('import_file', $this->t('The file could not be uploaded.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $filename = $form_state->getValue('import_file');

    if (file_exists($filename) && ($xml = simplexml_load_file($filename)) !== FALSE) {
      $batch_builder = (new BatchBuilder())
        ->setTitle($this->t('Importing comments and users'))
        ->setFinishCallback('\Drupal\apidrupalorg\Form\ImportForm::finishedBatchCreation')
        ->setInitMessage($this->t('Batch is starting...'))
        ->setProgressMessage($this->t('Processed @current out of @total'))
        ->setErrorMessage($this->t('Batch has encountered an error'));

      if ($xml->count() > 0 && !empty($xml->comment)) {
        foreach ($xml->comment as $comment) {
          // Process XML information.
          $comment_array = (array) $comment;

          try {
            $url = (string) (new \SimpleXMLElement($comment->url))['href'];
          }
          catch (\Throwable $e) {
            $url = NULL;
          }

          if (!empty($url)) {
            // Fields with CDATA or potentially empty.
            $comment_array['email'] = (string) $comment->email;
            $comment_array['body'] = (string) $comment->body;
            $comment_array['url'] = urldecode($url);

            $batch_builder->addOperation(
              '\Drupal\apidrupalorg\Form\ImportForm::upsertEntry',
              [$comment_array]
            );
          }
        }

        batch_set($batch_builder->toArray());
      }
    }
  }

  /**
   * Call to run when finishing creating everything.
   */
  public static function finishedBatchCreation() {
    $messenger = \Drupal::messenger();
    $messenger->addStatus(t('Comments and users were processed. See log messages for further information.'));
  }

  /**
   * Create or update a comment and user.
   *
   * @param array $comment_info
   *   Information about the comment to import.
   * @param array $context
   *   Context information.
   *
   * @return bool
   *   Whether the comment and user were created or not.
   */
  public static function upsertEntry(array $comment_info, array &$context) {
    if (empty($context['results'])) {
      $context['results']['comments_map'] = [];
    }

    // Special case for external documentation entries.
    if (strpos($comment_info['url'], '/api/drupal/developer') === 0) {
      $comment_info['url'] = str_replace('/api/drupal/developer', '/api/drupal/external_documentation!developer', $comment_info['url']);
    }

    // Find DocBlock as it should exist on the site already.
    $docBlock = self::getDocBlockFromUrl($comment_info['url']);
    if ($docBlock) {
      // Try to find the user by username, and create it if not found.
      // There is no need to worry about fields or roles as those will be synced
      // by the drupalorg_crosssite module.
      $user = user_load_by_mail($comment_info['email']);
      $user = $user ?: user_load_by_name($comment_info['username']);
      if (!$user) {
        $user = User::create()
          ->setUsername($comment_info['username'])
          ->setEmail($comment_info['email'])
          ->setPassword(\Drupal::service('password_generator')->generate())
          ->enforceIsNew()
          ->activate();
        $user->save();
      }

      if ($user) {
        // Find comment and create if not found.
        $properties = [
          'subject' => $comment_info['subject'],
          'entity_id' => $docBlock->id(),
          'entity_type' => $docBlock->getEntityTypeId(),
          'comment_type' => 'api_comment',
          'status' => $comment_info['status'],
          'field_api_comment_body' => [
            'value' => $comment_info['body'],
            'format' => 'filtered_html',
          ],
          'uid' => $user->id(),
        ];

        $comments = \Drupal::entityTypeManager()
          ->getStorage('comment')
          ->loadByProperties($properties);
        if (empty($comments)) {
          $values = $properties + [
            'field_name'  => 'comments',
            'created' => strtotime($comment_info['date']),
            'changed' => strtotime($comment_info['date']),
          ];

          // Thread?
          if ($comment_info['parent_cid']) {
            // We should have already processed the parent.
            $parent = $context['results']['comments_map'][$comment_info['parent_cid']] ?? 0;
            $values['pid'] = $parent;
          }

          $comment = Comment::create($values);
          $comment->save();
        }
        else {
          $comment = array_shift($comments);
        }

        // Map to keep D7 cid with the new comment id.
        $context['results']['comments_map'][$comment_info['cid']] = $comment->id();
      }
    }
    else {
      \Drupal::logger('apidrupalorg')->notice(t('@url not found for D7 comment with CID: @cid', [
        '@url' => $comment_info['url'],
        '@cid' => $comment_info['cid'],
      ]));
    }

    return FALSE;
  }

  /**
   * Tries to find the DocBlock object based on the url string of it.
   *
   * @param string $url
   *   Url of the Docblock object.
   *
   * @return \Drupal\api\Interfaces\DocBlockInterface|null
   *   DocBlock object or null.
   *
   * @see \Drupal\api\Formatter::objectUrl
   */
  protected static function getDocBlockFromUrl($url) {
    $docBlock_id = NULL;
    $parts = explode('/', trim($url, '/'));

    // There are two cases on Formatter::objectUrl, as follow:
    // File => /api/PROJECT/FILE_PATH/BRANCH.
    if (count($parts) == 4) {
      [, $project, $path, $branch] = $parts;
      $project = Project::getBySlug($project);
      $branch = $project ? Branch::getBySlug($branch, $project) : NULL;
      $path = Formatter::getReplacementName($path, 'file', TRUE);
      $docBlock_id = $branch ? DocBlock::findFileByFileName($path, $branch) : NULL;
    }
    // Object => /api/PROJECT/FILE_PATH/OBJECT_TYPE/OBJECT_NAME/BRANCH.
    elseif (count($parts) == 6) {
      [, $project, $path, $type, $name, $branch] = $parts;
      $project = Project::getBySlug($project);
      $branch = $project ? Branch::getBySlug($branch, $project) : NULL;
      $docBlock_id = $branch ? DocBlock::findByNameAndType($name, $type, $branch) : NULL;
    }

    if ($docBlock_id) {
      if (is_array($docBlock_id)) {
        $docBlock_id = array_shift($docBlock_id);
      }

      return DocBlock::load($docBlock_id);
    }

    return NULL;
  }

}
