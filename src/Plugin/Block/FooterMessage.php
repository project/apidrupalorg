<?php

namespace Drupal\apidrupalorg\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Provides a 'Footer message' Block for api.drupal.org site.
 *
 * @Block(
 *   id = "apidrupalorg_footer_message",
 *   admin_label = @Translation("api.drupal.org footer message"),
 * )
 */
class FooterMessage extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $markup = $this->t('<p>All source code and documentation on this site is released under the terms of the @license_link. @drupal_org_link is a @trademark_link of @dries_link.</p>', [
      '@license_link' => Link::fromTextAndUrl(
        $this->t('GNU General Public License, version 2 and later'),
        Url::fromUri('https://api.drupal.org/api/file/core!LICENSE.txt')
      )->toString(),
      '@drupal_org_link' => Link::fromTextAndUrl(
        $this->t('Drupal'),
        Url::fromUri('https://www.drupal.org/')
      )->toString(),
      '@trademark_link' => Link::fromTextAndUrl(
        $this->t('registered trademark'),
        Url::fromUri('https://drupal.com/trademark')
      )->toString(),
      '@dries_link' => Link::fromTextAndUrl(
        $this->t('Dries Buytaert'),
        Url::fromUri('https://dri.es')
      )->toString(),
    ]);
    return [
      '#markup' => $markup,
    ];
  }

}
