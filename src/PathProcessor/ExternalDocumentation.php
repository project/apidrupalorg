<?php

namespace Drupal\apidrupalorg\PathProcessor;

use Drupal\api\Formatter;
use Drupal\Core\PathProcessor\InboundPathProcessorInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Path processor for external documentation paths.
 */
class ExternalDocumentation implements InboundPathProcessorInterface {

  /**
   * Process any inbound URL and if it fits the pattern, transform it.
   *
   * @param string $path
   *   Current path being checked.
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Request object.
   *
   * @return string
   *   Transformed path so the router can consume it without issues.
   */
  public function processInbound($path, Request $request) {
    if (strpos($path, '/api/drupal/developer') === 0) {
      $path = str_replace('/api/drupal/developer', '/api/drupal/external_documentation!developer', $path);
    }

    return $path;
  }

}
